<?php
/**
 * Created by PhpStorm.
 * User: Matar
 * Date: 11.04.2019
 * Time: 23:59
 */

namespace App\Services;
use Illuminate\Support\Facades\Storage;
use Nathanmac\Utilities\Parser\Facades\Parser;
use GuzzleHttp\Exception\RequestException;
class CurrencyService
{
    private $except=['@ID','NumCode','CharCode','Nominal'];
    private $result=[];
    private $currencyValue=[];
    private $address='http://www.cbr.ru/scripts/XML_daily.asp';
    public function __construct()
    {
        $this->getCurrencys();
        $this->sanitazeArray($this->currencyValue);
    }
    private function getCurrencys(){
        Storage::disk('local')->put('file.xml', '');
        try {
            $guzzleClient = new \GuzzleHttp\Client();
            $response = $guzzleClient->get($this->address);
        } catch (RequestException $ex) {
            abort(404, 'resource not found');
        }
        $body = $response->getBody();
        $body->seek(0);
        $size = $body->getSize();
        $file = $body->read($size);
        Storage::disk('local')->put('file.xml', $file);
        Parser::payload('application/json');
        $contents = Storage::get('file.xml');
        $payload=Parser::xml($contents);
        $this->currencyValue=$payload["Valute"];
        Storage::delete('file.xml');
    }
    private function sanitazeArray(array $array){
        foreach ($array as $key=>$subarray){
            foreach ($subarray as $k=>$v){
                if (in_array($k,$this->except)){
                    unset($subarray[$k]);
                }else{
                    if($k=='Value'){
                        $subarray['rate'] = $subarray['Value'];
                        unset($subarray['Value']);
                    }else{
                        $subarray[strtolower($k)] = $subarray[$k];
                        unset($subarray[$k]);
                    }
                }
            }
            $this->result[]=$subarray;
        }
    }
    public function getResult(){
        $result=$this->result;
        return $result;
    }
}