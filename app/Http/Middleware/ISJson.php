<?php

namespace App\Http\Middleware;

use Closure;

class ISJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->isJson()) {
            return response()->json(['error'=>'This method accepts only json string']);
        }
        return $next($request);
    }
}
