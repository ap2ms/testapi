<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Currency;
class CurrencyController extends Controller
{
    public function index($perPage){
        $result = Currency::paginate($perPage);
        return response()->json(['status'=>200,'result'=>$result]);
    }
    public function show($id){
        $result = Currency::findOrFail($id);
        return response()->json(['status'=>200,'result'=>$result]);
    }
}
