<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1','middleware' => ['api','return-json','throttle:6,1']], function () {
    Route::post('login', 'AuthController@login');//->middleware('is_json');//не уверен что это нужно но можно ограничить только по типу контента json
    Route::post('signup', 'AuthController@signup');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('currencies/{per_page}','CurrencyController@index')->where('per_page','[0-9]+');
        Route::get('currency/{id}','CurrencyController@show')->where('id','[0-9]+');
    });
});

